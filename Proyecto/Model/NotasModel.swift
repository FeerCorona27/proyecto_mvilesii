//
//  NotasModel.swift
//  Proyecto
//
//  Created by Nathalia on 5/25/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import Foundation
struct NotasModel : Codable {
    let id_nota : Int16
    let nombre_nota : String?
    let contenido_nota : String?
    let id_libreta : Int16
    let fecha_creacion : String?
    let fecha_ultima_actualizacion : String?
    let id_usuario : Int16
   
    
    enum CodingKeys: String, CodingKey {
        case id_nota = "id_nota"
        case nombre_nota = "nombre_nota"
        case contenido_nota = "contenido_nota"
        case id_libreta = "id_libreta"
        case fecha_creacion = "fecha_creacion"
        case fecha_ultima_actualizacion = "fecha_ultima_actualizacion"
        case id_usuario = "id_usuario"
    }
  
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id_nota = try values.decodeIfPresent(Int16.self, forKey: .id_nota)!
        nombre_nota = try values.decodeIfPresent(String.self, forKey: .nombre_nota)
        contenido_nota  = try values.decodeIfPresent(String.self, forKey: .contenido_nota )
        id_libreta = try values.decodeIfPresent(Int16.self, forKey: .id_libreta)!
        fecha_creacion = try values.decodeIfPresent(String.self, forKey: .fecha_creacion)
        fecha_ultima_actualizacion = try values.decodeIfPresent(String.self, forKey: .fecha_ultima_actualizacion)
        id_usuario = try values.decodeIfPresent(Int16.self, forKey: .id_usuario)!
    }
    
}

