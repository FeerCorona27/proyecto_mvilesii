//
//  LibroMoldel.swift
//  Proyecto
//
//  Created by Nathalia on 5/26/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import Foundation
struct LibroModel : Codable {
    let id_libreta : Int16
    let nombre_nota : String?
    let id_usuario : Int16
    let color1 : Int16
    let color2 : Int16
    let color3 : Int16
       
    enum CodingKeys: String, CodingKey {
        case id_libreta = "id_libreta"
        case nombre_nota = "nombre_nota"
        case id_usuario = "id_usuario"
        case color1 = "color1"
        case color2 = "color2"
        case color3 = "color3"
    }
    
       
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id_libreta = try values.decodeIfPresent(Int16.self, forKey: .id_libreta)!
        nombre_nota = try values.decodeIfPresent(String.self, forKey: .nombre_nota)
        id_usuario = try values.decodeIfPresent(Int16.self, forKey: .id_usuario)!
        color1 = try values.decodeIfPresent(Int16.self, forKey: .color1)!
        color2 = try values.decodeIfPresent(Int16.self, forKey: .color2)!
        color3 = try values.decodeIfPresent(Int16.self, forKey: .color3)!
    }
}
