//
//  UserModel.swift
//  Proyecto
//
//  Created by Nathalia on 5/3/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import Foundation
struct UserModel : Codable {
    let id_usuario : Int?
    let password : String?
    let nombreuser : String?
    let correo : String?
    let tipo : String?
       
    
    enum CodingKeys: String, CodingKey {
        case id_usuario = "id_usuario"
        case password = "password"
        case nombreuser = "nombreuser"
        case correo = "correo"
        case tipo = "tipo"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id_usuario = try values.decodeIfPresent(Int.self, forKey: .id_usuario)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        nombreuser = try values.decodeIfPresent(String.self, forKey: .nombreuser)
        correo = try values.decodeIfPresent(String.self, forKey: .correo)
        tipo = try values.decodeIfPresent(String.self, forKey: .tipo)
    }
    
}
