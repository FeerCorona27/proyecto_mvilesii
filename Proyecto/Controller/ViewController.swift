//
//  ViewController.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 19/02/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnGet: UIButton!
    
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    var viewModelUser = UserViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.color = .systemPink
        activity.hidesWhenStopped = true
        
        
    }
    
   
    
    
    @IBAction func get(_ sender: Any) {
       
        
        viewModelUser.vc = self
        
        if txtNombre.text == "" || txtPassword.text == "" {
            txtNombre.text = ""
            txtPassword.text = ""
            
            let alert = UIAlertController(title: "Error!", message: "LLena todos los campos.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true)
        }else{
            self.activity.startAnimating()
            let parameters: [String : Any] = [
                "nombre": txtNombre.text!,
                "password": txtPassword.text!
            ]
            
            viewModelUser.getAllUserDataAF(parameters: parameters)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                
                self.validacionLogin()
            })
            
            
            
            
            
        }
    }
    
    
    func validacionLogin(){
        
        if viewModelUser.arrUsers.count == 0{
            txtNombre.text = ""
            txtPassword.text = ""
            
            let alert = UIAlertController(title: "Error!", message: "El usuario no existe.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }else{
            let modelUser = viewModelUser.arrUsers[0]
            print(viewModelUser.arrUsers[0])
            
            
            if modelUser.password != txtPassword.text {
                txtNombre.text = ""
                txtPassword.text = ""
                
                let alert = UIAlertController(title: "Error!", message: "Contrasena incorrecta.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                self.present(alert, animated: true)
                
            }else{
                UserDefaults.standard.set(viewModelUser.arrUsers[0].id_usuario , forKey: "id_usuario")
                UserDefaults.standard.set(viewModelUser.arrUsers[0].nombreuser , forKey: "nombre")
                UserDefaults.standard.set(viewModelUser.arrUsers[0].correo , forKey: "correo")
                UserDefaults.standard.set(viewModelUser.arrUsers[0].tipo , forKey: "tipo")
                performSegue(withIdentifier: "ingresar", sender: self)
                txtNombre.text = ""
                txtPassword.text = ""
                
            }
            
        }
        self.activity.stopAnimating()
        
    }
    
}


