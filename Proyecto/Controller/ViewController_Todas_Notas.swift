//
//  ViewController_Todas_Notas.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 31/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit
import CoreData

class ViewController_Todas_Notas: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    
    
    //Notas guardadas
    var notas = [Nota]()
    var notasAPI = [NotasModel]()
    
    //Tipo de usuario
    let tipo=UserDefaults.standard.object(forKey: "tipo")as? String
    
    // String que indica si es TOdas las notas o solo de una libreta
    var titulo_pantalla = ""
    
    //Id libreta para mostrar solo libretas pertenecientes a esa libreta
    var id_libreta: Int16 = 0
    
    //Objeto a nuestra clase Core Data que contola la lectura y escritura de datos en nuestro dispositivo
    private let manager = CoreDataManager()
    var viewModelUser = UserViewModel()
    
    
    // Titulo (LABEL) que indica si es TOdas las notas o solo de una libreta
    @IBOutlet weak var titulo_c: UILabel!
    @IBOutlet weak var table_view: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titulo_c.text = titulo_pantalla
        if (tipo == "Premium"){
            let parameters: [String : Any] = [
                                "id_libreta": id_libreta as? Int16 as Any]
            viewModelUser.selectNotaEspecificas(parameters: parameters, context: self)
            var helloWorldTimer = Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(ViewController_Mis_Notas.sayHello), userInfo: nil, repeats: true)
        }else{
          notas = manager.getNotas_Notebook(id: id_libreta)
        }
        
        
    }
    @objc func sayHello()
          {
             let parameters: [String : Any] = [
                                 "id_libreta": id_libreta as? Int16 as Any]
            viewModelUser.arrNotas.removeAll()
             viewModelUser.selectNotaEspecificas(parameters: parameters, context: self)
              print("hello World")
          }
    
    
   
    func reload() {
        if (tipo == "Premium"){
            let parameters: [String : Any] = ["id_libreta": id_libreta as? Int16 as Any]
            viewModelUser.arrNotas.removeAll()
            viewModelUser.selectNotaEspecificas(parameters: parameters,context: self)
            table_view.reloadData()
            
        }else{
            self.notas = manager.getNotas_Notebook(id: id_libreta)
            self.table_view.reloadData()
        }
        
    }
    
    
    
    
    
    func notasLength()-> Int{
        if (tipo == "Premium"){
        return notasAPI.count
        }else{
           return notas.count
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        performSegue(withIdentifier: "homework_two", sender: (indexPath.row))
        
    }
    
    
    //Numero de notas mas uno (celda para agregar)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1+notasLength()
    }
    
    // Eliminar Nota
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        print("Index-----------\(indexPath.row)")
        if editingStyle == .delete  {
            if (tipo == "Premium"){
                let parameters: [String : Any] = ["id_nota": self.notasAPI[indexPath.row-1].id_nota as? Int16 as Any]
                viewModelUser.borrarNota(parameters: parameters)
                viewModelUser.arrNotas.removeAll()
                let parameters2: [String : Any] = ["id_libreta": id_libreta as? Int16 as Any]
                viewModelUser.selectNotaEspecificas(parameters: parameters2,context: self)
                
            }else{
                manager.deleteNote(id: Int16(self.notas[indexPath.row-1].id_nota))
                self.notas =  self.manager.getNotas_Notebook(id: id_libreta)
                self.table_view.reloadData()
            }
            
            
        }
    }
    
    //Editar estilo para no permitir que se elimine el item de agregar
    public func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
    {
        //No permitir item de agregar nota
        if indexPath.row == 0 {
            return UITableViewCell.EditingStyle.none
        } else {
            return UITableViewCell.EditingStyle.delete
        }
    }
    
    
    //Estilio de Cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        //Celda Base
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell_tn", for: indexPath) as!  TableViewCell_Todas_Notas
        
        //Si es la celda de agregar
        if (indexPath.row + indexPath.section) == 0 {
            //No lleva datos especiales y se regresa
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell_add", for: indexPath) as!  TableViewCell_Todas_Notas
            return cell
        }
        if (tipo == "Premium"){
            cell.nombre.text = notasAPI[indexPath.row-1].nombre_nota
            cell.contenido.text = notasAPI[indexPath.row-1].contenido_nota
            let formatter3 = DateFormatter()
            formatter3.dateFormat = "HH, d MMM "
            //cell.fecha.text = formatter3.string(from: notasAPI[indexPath.row-1].fecha_ultima_actualizacion!)
            cell.layer.cornerRadius = 10
        }else{
            cell.nombre.text = notas[indexPath.row-1].nombre_nota
            cell.contenido.text = notas[indexPath.row-1].contenido_nota
            let formatter3 = DateFormatter()
            formatter3.dateFormat = "HH, d MMM "
            cell.fecha.text = formatter3.string(from: notas[indexPath.row-1].fecha_ultima_actualizacion!)
            cell.layer.cornerRadius = 10
        }
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier! == "homework_two"  {
            let note : ViewController_Nota = segue.destination  as! ViewController_Nota
            
            let index = (sender as! Int)
            
            
            if index > 0 {
                
                note.option = -1
                if(tipo == "Premium"){
                    note.change(titulo: notasAPI[index-1].nombre_nota!, contenido: notasAPI[index-1].contenido_nota!, contexto: self)
                    note.id_libreta = self.id_libreta
                    note.id_nota = notasAPI[index-1].id_nota
                }else{
                    note.change(titulo: notas[index-1].nombre_nota!, contenido: notas[index-1].contenido_nota!, contexto: self)
                    note.id_libreta = self.id_libreta
                    note.id_nota = notas[index-1].id_nota
                }
                
                
                
            }
            else{
                
                note.option = 0
                note.change(titulo: "Titulo Nota", contenido: "Contenido Nota", contexto: self)
                note.id_libreta = self.id_libreta
            }
            
            
        }
    }
    
    
    
    
    
    ///////////  D I S E Ñ O  D E  C E L D A
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        view.backgroundColor = .white
        return view
    }
    
}
