//
//  ViewController_Todas_Libretas.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 31/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit
import CoreData
import Foundation


class ViewController_Todas_Libretas: UIViewController ,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    
    //Bandera para elegir si es clik o eliminar libreta
    var status = false
    
    var libretas = [Libreta]()
    var libretasAPI = [LibroModel]()
    var viewModelUser = UserViewModel()
    let tipo=UserDefaults.standard.object(forKey: "tipo")as? String
    private let manager = CoreDataManager()
    var parametersID: [String : Any] = [
                             "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any]
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        if (tipo == "Premium"){
            viewModelUser.selectLibros(parameters: parametersID, context: self)
            var helloWorldTimer = Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(ViewController_Mis_Notas.sayHello), userInfo: nil, repeats: true)
            
        }else{
          libretas = manager.getNotebooks()
            
        }
        
        
    }
    
    @objc func sayHello()
       {
          viewModelUser.arrLibros.removeAll()
          viewModelUser.selectLibros(parameters: parametersID, context: self)
           print("hello World")
       }
    
    @IBOutlet weak var borrar: UIBarButtonItem!
    @IBOutlet weak var collection_view: UICollectionView!
    
    
    //Borrar cuando las libretas estan vibrando
    @IBAction func click_borrar(_ sender: Any) {
        
        // Si esta seleccionado Editar cambia a Hecho
        if status == false {
            status = true
            self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: "Hecho", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.click_borrar(_:)))
            
        }
        else {
            status = false
            self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: "Editar", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.click_borrar(_:)))
        }
        // Recargar Datos
        collection_view.reloadData()
        
        
    }
    
   
    
    
    
   
    
   
    //Get numero de libretas guardadas
    func notebooksLength()-> Int{
        if (tipo == "Premium"){
            return libretasAPI.count

        }else{
            return libretas.count

        }
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1+notebooksLength()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_tl", for: indexPath) as! CollectionViewCell_Todas_Libretas
        
        if (indexPath.row + indexPath.section) == 0{
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_add", for: indexPath) as! CollectionViewCell_Todas_Libretas
            return cell
        }
        if (tipo == "Premium"){
            cell.nombre_libreta.text = libretasAPI[indexPath.row-1].nombre_nota
            let red = CGFloat(libretasAPI[indexPath.row-1].color1)
            let green = CGFloat(libretasAPI[indexPath.row-1].color2)
            let blue = CGFloat(libretasAPI[indexPath.row-1].color3)
            
            cell.libreta.tintColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
        }else{
            cell.nombre_libreta.text = libretas[indexPath.row-1].nombre_nota
            let red = CGFloat(libretas[indexPath.row-1].color1)
            let green = CGFloat(libretas[indexPath.row-1].color2)
            let blue = CGFloat(libretas[indexPath.row-1].color3)
            
            cell.libreta.tintColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
        }
        
        if status == true {
            cell.borrar_boton.isHidden = false
            cell.shakeIcons()
        } else {
            cell.borrar_boton.isHidden = true
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        // Si el click fue en en la Celda para agregar  libretas
        if (indexPath.row + indexPath.section) == 0 {
            //Alerta para pedir el nombre de la libreta
            let alert = UIAlertController(title: "Nueva libreta", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            alert.addTextField(configurationHandler: { textField in
                textField.placeholder = "Ingresa nombre de libreta..."
            })
            alert.addAction(UIAlertAction(title: "Crear", style: .default, handler: { action in
                
                //Si ingreso el nombre de la libreta
                if let name = alert.textFields?.first?.text {
                    
                    if(self.tipo == "Premium"){
                        
                                   let parameters: [String : Any] = [
                                       "nombre_nota": name,
                                       "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any ,
                                       "color1": Int16(CGFloat(Int(arc4random_uniform(155))+100)),
                                       "color2": Int16(CGFloat(Int(arc4random_uniform(155))+100)),
                                       "color3": Int16(CGFloat(Int(arc4random_uniform(155))+100)),
                                      ]
                        
                        self.viewModelUser.insertLibro(parameters: parameters)
                        self.viewModelUser.arrLibros.removeAll()
                        self.viewModelUser.selectLibros(parameters: self.parametersID, context: self)
                        
                                   
                    }else{
                       self.manager.addNotebooks(nombre: name)
                       self.libretas = self.manager.getNotebooks()
                       self.collection_view.reloadData()
                    }
                    
                }
                
               
            }))
            self.present(alert, animated: true)
        }
            //El click fue en una libreta
        else{
            
            
            // Si esta activado la opcion de Eliminar
            if status == true {
                
                if (tipo=="Premium"){
                    let parameters: [String : Any] = [
                        "id_libreta": self.libretasAPI[indexPath.row-1].id_libreta as Any
                    ]
                   
                    viewModelUser.borrarLibro(parameters:parameters, context: self)
                    viewModelUser.arrLibros.removeAll()
                    viewModelUser.selectLibros(parameters: self.parametersID, context: self)
                }else{
                    manager.deleteNotebooks(id: self.libretas[indexPath.row-1].id_libreta)
                    self.libretas = self.manager.getNotebooks()
                    self.collection_view.reloadData()
                }
               
            }
            else{
                performSegue(withIdentifier: "notas_libreta", sender: indexPath.row)
            }
        }
        
        
        
        
        
        
        
        
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier! == "notas_libreta" {
            
            let index = (sender as! Int)
            let notebook : ViewController_Todas_Notas = segue.destination  as! ViewController_Todas_Notas
            
            if (tipo == "Premium") {
                 notebook.titulo_pantalla = " \(libretasAPI[index-1].nombre_nota!)"
                 notebook.id_libreta = libretasAPI[index-1].id_libreta
            }else{
                notebook.titulo_pantalla = " \(libretas[index-1].nombre_nota!)"
                notebook.id_libreta = libretas[index-1].id_libreta
            }
        }
        
    }
    
    
    
    
    
    
    
    //Tamaño celdas
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screen = UIScreen.main.bounds
        
        let withValue = (screen.width / 2.0) 
        return CGSize(width: withValue, height: withValue)
    }
    
    
    
    
    
    
    
}
