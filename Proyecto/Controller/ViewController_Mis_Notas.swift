//
//  ViewController_Mis_Notas.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 30/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit
import Foundation

class ViewController_Mis_Notas: UIViewController , UITableViewDelegate, UITableViewDataSource,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    
    @IBOutlet weak var tableView: UITableView!

    
    @IBOutlet weak var collectionView: UICollectionView!
    let tipo=UserDefaults.standard.object(forKey: "tipo")as? String
    var viewModelUser = UserViewModel()
    var libretas = [Libreta]()
    var notas = [Nota]()
    var libretasAPI = [LibroModel]()
    var notasAPI = [NotasModel]()
     //var viewModelUser = UserViewModel()
    private let manager = CoreDataManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let parameters: [String : Any] = [
                     "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any]
        
     //   navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Cerrar Sesion", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cerrarSesion))
        if(tipo == "Premium"){
            viewModelUser.selectLibros(parameters: parameters, context: self)
            viewModelUser.selectNotaDeterminadas(parameters: parameters, context: self)
            var helloWorldTimer = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(ViewController_Mis_Notas.sayHello), userInfo: nil, repeats: true)
          
            
        }else{
            libretas = manager.getNotebooks()
            notas = manager.getNotas(cuantas: 5)
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func cerrarSesion (){
        let alertController = UIAlertController(title: "Cerrar Sesion", message: "Estas seguro de actualizar el password", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            self.viewModelUser.arrUsers.removeAll()
        }
        alertController.addAction(OKAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
            
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    
    @objc func sayHello()
    {
        let parameters: [String : Any] = [
        "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any]
        viewModelUser.arrLibros.removeAll()
        viewModelUser.arrLibros.removeAll()
        viewModelUser.selectLibros(parameters: parameters, context: self)
        viewModelUser.selectNotaDeterminadas(parameters: parameters, context: self)
        print("hello World")
    }
    
    
    
    
    
    /////////////////Actions
    
    
    // Id  Segue settings
    @IBAction func settings(_ sender: Any) {
        performSegue(withIdentifier: "settings", sender: self)
    }
    
    
    // Id segue notebooks
    @IBAction func all_notebooks(_ sender: Any) {
        // performSegue(withIdentifier: "notebooks", sender: self)
    }
    
    
    //Id segue notes
    @IBAction func all_notes(_ sender: Any) {
        //performSegue(withIdentifier: "notes", sender: self)
    }
    
    
    /////////////////////
    
    
    
    
    
    
    
    
    
    
    
    //////////////////// T A B L E       V I E W
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if (tipo == "Premium"){
           return notasAPI.count
            
        }else{
         return notas.count
        }
    
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as!  TableViewCell_Notas_Mis_Notas
        
            if (self.tipo == "Premium") {
                cell.titulo_nota.text = self.notasAPI[indexPath.row+indexPath.section].nombre_nota
                cell.contenido_nota.text = self.notasAPI[indexPath.row+indexPath.section].contenido_nota
                let formatter3 = DateFormatter()
                formatter3.dateFormat = "HH, d MMM "
                //cell.fecha_nota.text  = formatter3.string(from: self.notasAPI[indexPath.row+indexPath.section].fecha_ultima_actualizacion!)
                cell.layer.cornerRadius = 10
            }else{
                cell.titulo_nota.text = self.notas[indexPath.row+indexPath.section].nombre_nota
                cell.contenido_nota.text = self.notas[indexPath.row+indexPath.section].contenido_nota
                let formatter3 = DateFormatter()
                formatter3.dateFormat = "HH, d MMM "
                cell.fecha_nota.text  = formatter3.string(from: self.notas[indexPath.row+indexPath.section].fecha_ultima_actualizacion!)
                cell.layer.cornerRadius = 10
            }
        
       
        
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        view.backgroundColor = .white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "note", sender: (indexPath.row+indexPath.section))
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    ////////////////////    C O L L E C T I O N  V  I E W
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     
        if (tipo == "Premium"){
            return self.libretasAPI.count
            
        }
            return libretas.count
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell_c", for: indexPath) as! CollectionViewCell_Notas_Mis_Libretas
        
            if (self.tipo == "Premium") {
                cell.label_libreta.text = "\(self.libretasAPI[indexPath.row].nombre_nota!)"
                let red = CGFloat(self.libretasAPI[indexPath.row].color1)
                let green = CGFloat(self.libretasAPI[indexPath.row].color2)
                let blue = CGFloat(self.libretasAPI[indexPath.row].color3)
            cell.libreta.tintColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
        }else{
                cell.label_libreta.text = "\(self.libretas[indexPath.row].nombre_nota!)"
                let red = CGFloat(self.libretas[indexPath.row].color1)
                let green = CGFloat(self.libretas[indexPath.row].color2)
                let blue = CGFloat(self.libretas[indexPath.row].color3)
             cell.libreta.tintColor = UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
            
        }
        
        
        
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screen = UIScreen.main.bounds
        
        let withValue = (screen.width / 4.0)
        return CGSize(width: withValue, height: withValue)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "notebook", sender: indexPath.row)
    }
    
    
    
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("hola")
       
        switch  String(segue.identifier!) {
            
            
        case "settings":
            print ("settings")
            
        case "notes":
            
            let notes : ViewController_Todas_Notas = segue.destination  as! ViewController_Todas_Notas
            notes.titulo_pantalla = "Notas"
            notes.id_libreta = -1
            
            
            
            
        case "notebook":
            let index = (sender as! Int)
            let notebook : ViewController_Todas_Notas = segue.destination  as! ViewController_Todas_Notas
            if (self.tipo == "Premium") {
                notebook.titulo_pantalla = " \(self.libretasAPI[index].nombre_nota!)"
                notebook.id_libreta = self.libretasAPI[index].id_libreta
            }else{
                notebook.titulo_pantalla = " \(self.libretas[index].nombre_nota!)"
                notebook.id_libreta = self.libretas[index].id_libreta
            }
           
            
            
            
            
            
            
            
        case "notebooks":
            print("no hago nada")
            
            
            
            
        case "note":
            let index = (sender as! Int)
            let note : ViewController_Nota = segue.destination  as! ViewController_Nota
            note.before = 1
            if (self.tipo == "Premium"){
                note.change(titulo: self.notasAPI[index].nombre_nota!, contenido: self.notasAPI[index].contenido_nota!, contexto: ViewController_Todas_Notas())
                note.id_nota = self.notasAPI[index].id_nota
            }else{
                note.change(titulo: self.notas[index].nombre_nota!, contenido: self.notas[index].contenido_nota!, contexto: ViewController_Todas_Notas())
                note.id_nota = self.notas[index].id_nota
            }
           
            
            
            
            
        default  :
            print ("Ninguno")
            
            
            
        }
        
       
    }
    
    
}
