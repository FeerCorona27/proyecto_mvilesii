//
//  TableViewController_DataUser.swift
//  Proyecto
//
//  Created by Nathalia on 5/22/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit

class TableViewController_DataUser: UITableViewController {

    @IBOutlet weak var switchOutlet: UISwitch!
    @IBOutlet weak var lbTipo: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var textPassword: UITextField!
    
    var viewModelUser = UserViewModel()
  
    
  override func viewDidLoad() {
        super.viewDidLoad()
        
        switchOutlet.isOn = false
        let tipo=UserDefaults.standard.object(forKey: "tipo")as? String
        lbEmail.text=UserDefaults.standard.object(forKey: "correo")as? String
        lbTipo.text=tipo
        if tipo == "Premium" {
            switchOutlet.isOn = true
        }
       
    }
    
    @IBAction func actualizarPassword(_ sender: Any) {
        alertActualizarPassword()
    }
    
    @IBAction func switchSub(_ sender: UISwitch) {
        if (sender.isOn == true) {
            switchOutlet.isOn = false
            print("yes")
            performSegue(withIdentifier: "payment", sender: self)
        }else{
            
            alertCancelarSub()
        }
    }
    
    
    func alertCancelarSub (){
       let parameters: [String : Any] = [
              "id_usuario": UserDefaults.standard.object(forKey: "id_usuario") as? Int as Any]
        let alertController = UIAlertController(title: "Cancelar Subscripcion", message: "Deseas cancelar la subscripcion", preferredStyle: .alert)
               let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                print("Cancelando subscripcion");
                self.viewModelUser.updateTipoSubscripcionCancelar(parameters: parameters)
                     UserDefaults.standard.set("Free" , forKey: "tipo")
                   
               }
               alertController.addAction(OKAction)
               let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
                   print("Cancel button tapped");
                   self.switchOutlet.isOn = true
               }
               alertController.addAction(cancelAction)
               self.present(alertController, animated: true, completion:nil)
    }
    
    
    func alertActualizarPassword (){
        let parametersID: [String : Any] = [
                 "id_usuario": UserDefaults.standard.object(forKey: "id_usuario") as? Int as Any,
                 "password": textPassword.text!
            ]
        let alertController = UIAlertController(title: "Actualizar Password", message: "Estas seguro de actualizar el password", preferredStyle: .alert)
        textPassword.text = ""
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
       //  print("Cancelando subscripcion");
            self.viewModelUser.updatePassword(parameters: parametersID)
        }
        alertController.addAction(OKAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
            self.switchOutlet.isOn = true
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    
    
    
 

}
