//
//  ViewController_Nota.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 02/04/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit
import CoreData

class ViewController_Nota: UIViewController {
    let tipo=UserDefaults.standard.object(forKey: "tipo")as? String
    private let manager = CoreDataManager()
    var viewModelUser = UserViewModel()
    // if option == -1 update else add
    var option = 0
    
    //Id Libreta
    var id_libreta: Int16 = 0
    
    //De donde viene¿?
    var before = 0
    
    //
    var id_nota: Int16 = 0
    
    var ViewController_Todas_Notas: ViewController_Todas_Notas!
    
    @IBOutlet weak var titulo: UITextField!
    @IBOutlet weak var contenido: UITextView!
    
    var aux_titulo = ""
    var aux_contenido = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        titulo.text = aux_titulo
        contenido.text = aux_contenido
    }
    func change(titulo: String,contenido: String, contexto: ViewController_Todas_Notas) {
        self.ViewController_Todas_Notas = contexto
        aux_titulo = titulo
        aux_contenido = contenido
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            
            if option != -1{
                addNotes()
            }
            else{
                updateNota()
            }
            if before == 0{
                self.ViewController_Todas_Notas.reload()
            }
            
            
            
        }
    }
    
    
    func updateNota(){
        if(tipo == "Premium"){
            let parameters: [String : Any] = [
                           "id_nota": id_nota,
                           "nombre_nota": titulo.text!,
                           "contenido_nota": contenido.text!,
                           "fecha_ultima_actualizacion": NSDate.now
                       ]
            viewModelUser.updateNotas(parameters: parameters)
            
            
            
            
        }else{
            let context = manager.getContainer().viewContext
            let fetchRequest : NSFetchRequest<Nota> = Nota.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id_nota == %i ", id_nota)
            do {
                let result = try manager.getContainer().viewContext.fetch(fetchRequest)
                if result.count != 0{
                    var managedObject = result[0]
                    managedObject.setValue(titulo.text, forKey: "nombre_nota")
                    managedObject.setValue(contenido.text, forKey: "contenido_nota")
                    managedObject.setValue(NSDate.now, forKey:"fecha_ultima_actualizacion" )
                    try context.save()
                }
                
                } catch {
                       print("El error obteniendo usuario(s) \(error)")
                   }
        }
        
            
       
        
    }
    
    
    func addNotes(){
        if(tipo == "Premium"){
            let parameters: [String : Any] = [
                "nombre_nota": titulo.text!,
                "contenido_nota": contenido.text!,
                "id_libreta": self.id_libreta,
                "fecha_creacion": NSDate.now,
                "fecha_ultima_actualizacion": NSDate.now,
                "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any,
            ]
            
            viewModelUser.insertNota(parameters: parameters)
        }else{
            let context = manager.getContainer().viewContext
            let nota = Nota(context: context)
            nota.id_nota = manager.get_id_nota()
            nota.nombre_nota = titulo.text
            nota.contenido_nota = contenido.text
            nota.id_libreta = self.id_libreta
            nota.fecha_creacion = NSDate.now
            nota.fecha_ultima_actualizacion = NSDate.now
            do {
                       
                       try context.save()
                       print("Usuario \(nota) guardado")
                       
                   } catch {
                       
                       print("Error guardando Nota llamada — \(titulo)")
                   }
        }
        
        
        
        
       
    }
    
}
