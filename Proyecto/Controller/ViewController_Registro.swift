//
//  ViewController_Registro.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 30/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit

class ViewController_Registro: UIViewController {
    
    var viewModelUser = UserViewModel()
    
    
    @IBOutlet weak var txtNombre: UITextField!
    @IBOutlet weak var txtApellidopat: UITextField!
    @IBOutlet weak var txtApellidomat: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    @IBOutlet weak var txtNombreuser: UITextField!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var txtContrasena: UITextField!
    @IBOutlet weak var txtContrasena2: UITextField!
    @IBOutlet weak var btnCrear: UIButton!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var sexo2 = "F"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.color = .systemPink
        activity.hidesWhenStopped = true
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func sexoSelected(_ sender: Any) {
        
        switch segment.selectedSegmentIndex {
        case 0:
            sexo2 = "F"
            break
        case 1:
            sexo2 = "M"
            break
        default:
            break
        }
    }
    
    
    @IBAction func post(_ sender: Any) {
        activity.startAnimating()
        
        
        if  txtNombre.text == "" || txtNombreuser.text == "" || txtApellidomat.text == "" ||  txtApellidopat.text == "" || txtCorreo.text == "" || txtContrasena2.text == "" || txtContrasena.text == "" {
            
            txtContrasena.text = ""
            txtContrasena2.text = ""
            txtNombre.text = ""
            txtNombreuser.text = ""
            txtApellidomat.text = ""
            txtApellidopat.text = ""
            txtCorreo.text = ""
            
            let alert = UIAlertController(title: "Error!", message: "LLena todos los campos.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }else if txtContrasena.text != txtContrasena2.text {
            
            txtContrasena.text = ""
            txtContrasena2.text = ""
            
            let alert = UIAlertController(title: "Error!", message: "Contraseñas Incorrectas.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert, animated: true)
            
            
        }else{
            
            
            let parameters: [String : Any] = [
                "nombre": txtNombre.text!,
                "apellidomat": txtApellidomat.text!,
                "apellidopat": txtApellidopat.text!,
                "correo": txtCorreo.text!,
                "password": txtContrasena.text!,
                "nombreuser": txtNombreuser.text!,
                "sexo": sexo2,
                "tipo": "Free"]
            
            
            viewModelUser.insertUsersAF(parameters: parameters)
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                //Lo borre por que se crean mas pantallas , mejor que el usuario se regrese solo
                //self.performSegue(withIdentifier: "login", sender: self)
                let alert = UIAlertController(title: "Felicidades", message: "¡Usuario Registrado Correctamente!", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
                self.present(alert, animated: true)
                //Limpiar texto
                self.txtContrasena.text = ""
                self.txtContrasena2.text = ""
                self.txtNombre.text = ""
                self.txtNombreuser.text = ""
                self.txtApellidomat.text = ""
                self.txtApellidopat.text = ""
                self.txtCorreo.text = ""
                self.activity.stopAnimating()
            })
            
        }
        
    }
    
    
    
    
    
}
