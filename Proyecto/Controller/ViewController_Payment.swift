//
//  ViewController_Payment.swift
//  Proyecto
//
//  Created by Nathalia on 5/21/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit
import BraintreeUI
import BraintreePayPal
import SwiftUI

class ViewController_Payment: UIViewController {
    
    var braintreeClient: BTAPIClient!
    var libretas = [Libreta]()
    var notas = [Nota]()
    var viewModelUser = UserViewModel()
    private let manager = CoreDataManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.standard.object(forKey: "nombre")as? String as Any)
        //let stringOne = defaults.string(forKey: defaultsKeys.keyOne)
      //  print(ProcessInfo.processInfo.environment["arrUsuario"] as Any)
        braintreeClient = BTAPIClient(authorization: "sandbox_pgybdz7b_d5w589yrpnnqyp8h")
     
        
    }
    

   
  
    
    @IBAction func btnPay(_ sender: Any) {
        let parameters: [String : Any] = [
              "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any]
        let payPalDriver = BTPayPalDriver(apiClient: self.braintreeClient)
        payPalDriver.viewControllerPresentingDelegate = self as? BTViewControllerPresentingDelegate
        payPalDriver.appSwitchDelegate = self as? BTAppSwitchDelegate
             
             let request = BTPayPalRequest(amount: "2.60")
                    request.currencyCode = "USD" // Optional; see BTPayPalRequest.h for more options
                    payPalDriver.requestOneTimePayment(request) { (tokenizedPayPalAccount, error) in
                        if let tokenizedPayPalAccount = tokenizedPayPalAccount {
                            print("Got a nonce: \(tokenizedPayPalAccount.nonce)")
                     
                            //////////////////Actualizacion de tipo de subscripcion///////////////////////////
                            self.viewModelUser.updateTipoSubscripcion(parameters: parameters)
                            ///////////////////////Sincronizacion de libretas y notas//////////////////////////////
                            self.libretas = self.manager.getNotebooks()
                            print (self.libretas.count)
                            
                            for i in 0...self.libretas.count-1  {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
                                self.viewModelUser.selectLibrosAll()
                                let id_libreta = self.libretas[i].id_libreta
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                                    print("PPPP",self.viewModelUser.arrLibros.count)
                                    let libretasBD_Count = self.viewModelUser.arrLibros[self.viewModelUser.arrLibros.count-1].id_libreta+Int16(i+1)
                                   // if (i==0){libretasBD_Count+=1}
                                        let parameters: [String : Any] = [
                                            "nombre_nota": self.libretas[i].nombre_nota!,
                                            "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any ,
                                            "color1": Int16(CGFloat(Int(arc4random_uniform(155))+100)),
                                            "color2": Int16(CGFloat(Int(arc4random_uniform(155))+100)),
                                            "color3": Int16(CGFloat(Int(arc4random_uniform(155))+100)),
                                        ]
                                    self.viewModelUser.insertLibro(parameters: parameters)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: {
                                        self.notas = self.manager.getNotas_Notebook(id: id_libreta)
                                        var Notas = self.notas.count
                                        print("Notas", Notas)
                                        if (Notas != 0){
                                            Notas-=1
                                            for n in 0...Notas{
                                                let parametersNota: [String : Any] = [
                                                    "nombre_nota": self.notas[n].nombre_nota!,
                                                    "contenido_nota": self.notas[n].contenido_nota!,
                                                    "id_libreta": libretasBD_Count,
                                                    "fecha_creacion":  self.notas[n].fecha_creacion!,
                                                    "fecha_ultima_actualizacion": self.notas[n].fecha_ultima_actualizacion!,
                                                    "id_usuario": UserDefaults.standard.object(forKey: "id_usuario")as? Int as Any]
                                                    self.viewModelUser.insertNota(parameters: parametersNota)
                                            }
                                        }
                                    
                                    })
                                    
                                })
                            
                                }) //viewModelUser.selectNotaEspecificas(parameters: parameters, context: self)
                            }
                       
                            
                            
                            
                        print(parameters["id_usuario"] as Any)
                        UserDefaults.standard.set("Premium" , forKey: "tipo")
                        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                            // Access additional information
                            let email = tokenizedPayPalAccount.email
                            print(email)
                            let firstName = tokenizedPayPalAccount.firstName
                            let lastName = tokenizedPayPalAccount.lastName
                            let phone = tokenizedPayPalAccount.phone

                            // See BTPostalAddress.h for details
                            let billingAddress = tokenizedPayPalAccount.billingAddress
                            let shippingAddress = tokenizedPayPalAccount.shippingAddress
                        })
                        } else if let error = error {
                            // Handle error here...
                        } else {
                            // Buyer canceled payment approval
                        }
                        
                    }
                        
    }
    
}


extension ViewController : BTViewControllerPresentingDelegate{
    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        
    }
    
    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        
    }
    
    
    
}

extension ViewController : BTAppSwitchDelegate{
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        
    }
    
    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {
        
    }
    
    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        
    }
    
    
}
