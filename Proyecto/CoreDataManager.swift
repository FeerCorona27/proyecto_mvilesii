//
//  CoreDataManager.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 15/04/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager {
    
    
    private let container : NSPersistentContainer!
    
    init() {
        container = NSPersistentContainer(name: "Data")
        
        setupDatabase()
    }
    
    private func setupDatabase() {
        
        container.loadPersistentStores { (desc, error) in
            if let error = error {
                print("Error loading store \(desc) — \(error)")
                return
            }
            print("Database ready!")
        }
        
    }
    
    
    func getContainer()-> NSPersistentContainer{
        return self.container
    }
    
    
    
    func  get_id_libreta() -> Int16{
        let fetchRequest : NSFetchRequest<Libreta> = Libreta.fetchRequest()
        let sort = NSSortDescriptor(key: "id_libreta", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        do {
            
            
            let result = try getContainer().viewContext.fetch(fetchRequest)
            let firstObject = result.first as! NSManagedObject
            if let foundValue = firstObject.value(forKey: "id_libreta") as? NSNumber {
                return Int16(NSNumber.init(value: foundValue.intValue + 1))
            }
        } catch {
            return 0
        }
        
        return 0
    }
    
    func  get_id_nota() -> Int16{
        let fetchRequest : NSFetchRequest<Nota> = Nota.fetchRequest()
        let sort = NSSortDescriptor(key: "id_nota", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        do {
            
            
            let result = try getContainer().viewContext.fetch(fetchRequest)
            let firstObject = result.first as! NSManagedObject
            if let foundValue = firstObject.value(forKey: "id_nota") as? NSNumber {
                return Int16(NSNumber.init(value: foundValue.intValue + 1))
            }
        } catch {
            return 0
        }
        
        return 0
        
    }
    
    func getNotebooks() -> [Libreta]{
        
        let fetchRequest : NSFetchRequest<Libreta> = Libreta.fetchRequest()
        do {
            
            
            let result = try getContainer().viewContext.fetch(fetchRequest)
            
            return result
        } catch {
            print("El error obteniendo usuario(s) \(error)")
        }
        
        
        return []
    }
    
    func getNotas(cuantas num: Int) -> [Nota]{
        
        let fetchRequest : NSFetchRequest<Nota> = Nota.fetchRequest()
        let sort = NSSortDescriptor(key: "fecha_ultima_actualizacion", ascending: false)
        fetchRequest.sortDescriptors = [sort]
        do {
            
            
            var result = try getContainer().viewContext.fetch(fetchRequest)
            
            if result.count > 10{
                
                if num > 0{
                    return Array(result.prefix(num))
                    
                }
                else {
                    return result
                }
            }
            else{
                return result
            }
        } catch {
            print("El error obteniendo usuario(s) \(error)")
        }
        
        
        return []
        
    }
    
    func getNotas_Notebook(id id_libreta : Int16) -> [Nota]{
        
        let fetchRequest : NSFetchRequest<Nota> = Nota.fetchRequest()
        if id_libreta != -1{
            fetchRequest.predicate = NSPredicate(format: "id_libreta == %i ", id_libreta)
        }
        
        do {
            
            
            let result = try getContainer().viewContext.fetch(fetchRequest)
            
            return result
        } catch {
            print("El error obteniendo usuario(s) \(error)")
        }
        
        
        return []
        
    }
    
    
    func deleteNote(id notebook: Int16){
        let context = getContainer().viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Nota")
        //Se compara con el id que recibi
        fetchRequest.predicate = NSPredicate(format: "id_nota == %i ", notebook)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            
            
            try context.execute(deleteRequest)
            
            
            
        }catch {
            print("Error Borrando los usuarios \(error)")
        }
    }
    func deleteNotebooks(id notebook: Int16){
        
        
        
        let context = getContainer().viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Libreta")
        //Se compara con el id que recibi
        fetchRequest.predicate = NSPredicate(format: "id_libreta == %i ", notebook)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            
            
            try context.execute(deleteRequest)
            
        }catch {
            print("Error Borrando los usuarios \(error)")
        }
    }
   
    func addNotebooks(nombre notebook: String){
        
        let context = getContainer().viewContext
        
        let libreta = Libreta(context: context)
        libreta.id_libreta = get_id_libreta()
        libreta.nombre_nota = notebook
        let red = CGFloat(Int(arc4random_uniform(155))+100)
        let green = CGFloat(Int(arc4random_uniform(155))+100)
        let blue = CGFloat(Int(arc4random_uniform(155))+100)
        libreta.color1 = Int16(red)
        libreta.color2 = Int16(green)
        libreta.color3 = Int16(blue)
        
        
        
        do {
            try context.save()
            print("Usuario \(notebook) guardado")
            
        } catch {
            
            print("Error guardando usuario — \(notebook)")
        }
    }
    
    
    
}
