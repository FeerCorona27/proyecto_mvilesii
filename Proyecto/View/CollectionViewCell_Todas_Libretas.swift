//
//  CollectionViewCell_Todas_Libretas.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 31/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit

class CollectionViewCell_Todas_Libretas: UICollectionViewCell {
    @IBOutlet weak var add_libreta: UIView!
    
    @IBOutlet weak var borrar_boton: UIButton!
    @IBOutlet weak var libreta: UIImageView!
    @IBOutlet weak var nombre_libreta: UILabel!
    
    @IBOutlet weak var View: UIView!
    var shakeEnabled = false
    
    func shakeIcons() {
        let shakeAnim = CABasicAnimation(keyPath: "transform.rotation")
        shakeAnim.duration = 0.05
        shakeAnim.repeatCount = 2
        shakeAnim.autoreverses = true
        let startAngle: Float = (1) * 3.14159/180
        var stopAngle = -startAngle
        shakeAnim.fromValue = NSNumber(value: startAngle)
        shakeAnim.toValue = NSNumber(value: 3 * stopAngle)
        shakeAnim.autoreverses = true
        shakeAnim.duration = 0.2
        shakeAnim.repeatCount = 10000
        shakeAnim.timeOffset = 290 * drand48()
        
        self.View.layer.add(shakeAnim, forKey:"shaking")
        shakeEnabled = true
    }
    
    func stopShakingIcons() {
        let layer: CALayer = self.layer
        self.View.layer.removeAnimation(forKey: "shaking")
        borrar_boton.isHidden = true
        shakeEnabled = false
    }
    
    
}
