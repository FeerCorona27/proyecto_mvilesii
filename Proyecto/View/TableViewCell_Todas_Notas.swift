//
//  TableViewCell_Todas_Notas.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 31/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit

class TableViewCell_Todas_Notas: UITableViewCell {
    
    
    @IBOutlet weak var nombre: UILabel!
    
    @IBOutlet weak var nota: UIImageView!
    @IBOutlet weak var contenido: UILabel!
    @IBOutlet weak var fecha: UILabel!
    
    @IBOutlet weak var cell_add: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
