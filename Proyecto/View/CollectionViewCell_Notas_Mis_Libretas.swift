//
//  CollectionViewCell_Notas_Mis_Libretas.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 31/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit

class CollectionViewCell_Notas_Mis_Libretas: UICollectionViewCell {
    
    @IBOutlet weak var label_libreta: UILabel!
    @IBOutlet weak var libreta: UIImageView!
}
