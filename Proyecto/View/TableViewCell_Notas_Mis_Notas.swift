//
//  TableViewCell_Notas_Mis_Notas.swift
//  Proyecto
//
//  Created by Fernando Garcia Corona on 30/03/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit

class TableViewCell_Notas_Mis_Notas: UITableViewCell {

    @IBOutlet weak var titulo_nota: UILabel!
    @IBOutlet weak var fecha_nota: UILabel!
    @IBOutlet weak var contenido_nota: UILabel!
    @IBOutlet weak var contenedor: UIView!
    @IBOutlet weak var libreta: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
          super.setSelected(selected, animated: animated)
    
          // Configure the view for the selected state
       }

}
