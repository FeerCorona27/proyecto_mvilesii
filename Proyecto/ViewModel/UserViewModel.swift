//
//  UserViewModel.swift
//  Proyecto
//
//  Created by Nathalia on 5/3/20.
//  Copyright © 2020 Fernando Garcia Corona. All rights reserved.
//

import UIKit
import Alamofire

class UserViewModel {
    weak var vc: ViewController?
    var arrUsers = [UserModel]()
    var arrLibros = [LibroModel]()
    var arrNotas = [NotasModel]()
    

    
    
    func insertUsersAF(parameters: [String: Any]){
        AF.request("http://api-moviles-2.herokuapp.com/insertU", method: .post, parameters: parameters).response{
            response in
            debugPrint(response)
        }
    }
    
    func getAllUserDataAF(parameters: [String: Any]){
        
        AF.request("http://api-moviles-2.herokuapp.com/login",parameters: parameters).response{
            response in
            debugPrint(response)
            if let data = response.data {
                
                do{
                    let userResponse = try JSONDecoder().decode([UserModel].self, from: data)
                    self.arrUsers.append(contentsOf: userResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
    }
    
    func updateTipoSubscripcion (parameters: [String: Any]){
        AF.request("http://api-moviles-2.herokuapp.com/updateTipo", method: .post, parameters: parameters).response{
                   response in
                   debugPrint(response)
               }
        
    }
    
    func updateTipoSubscripcionCancelar (parameters: [String: Any]){
         AF.request("http://api-moviles-2.herokuapp.com/updateTipoCancelar", method: .post, parameters: parameters).response{
                    response in
                    debugPrint(response)
                }
         
     }
    
    
    func insertLibro (parameters: [String: Any]){
        AF.request("http://api-moviles-2.herokuapp.com/insertLibro", method: .post, parameters: parameters).response{
                   response in
                   debugPrint(response)
               }
        
        
    }
    
    func insertNota (parameters: [String: Any]){
        AF.request("http://api-moviles-2.herokuapp.com/insertNotas", method: .post, parameters: parameters).response{
                   response in
                   debugPrint("Respuesta---------------\(response)")
               }
        
    }
    
    func updatePassword (parameters: [String: Any]){
        AF.request("http://api-moviles-2.herokuapp.com/updatePassword", method: .post, parameters: parameters).response{
                   response in
                   debugPrint(response)
               }
        
        
    }

    func selectLibros (parameters: [String: Any], context: ViewController_Mis_Notas){
        AF.request("http://api-moviles-2.herokuapp.com/libros",parameters: parameters).response{
            response in
            debugPrint(response)
            if let data = response.data {
                
                do{
                    let libroResponse = try JSONDecoder().decode([LibroModel].self, from: data)
                    self.arrLibros.append(contentsOf: libroResponse)
                    context.libretasAPI = self.arrLibros
                    context.collectionView.reloadData()
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
        
        
    }
    func selectLibros (parameters: [String: Any], context: ViewController_Todas_Libretas){
        
        AF.request("http://api-moviles-2.herokuapp.com/libros",parameters: parameters).response{
            response in
            debugPrint(response)
            if let data = response.data {
                
                do{
                    let libroResponse = try JSONDecoder().decode([LibroModel].self, from: data)
                    self.arrLibros.append(contentsOf: libroResponse)
                    context.libretasAPI = self.arrLibros
                    context.collection_view.reloadData()
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
        
        
    }
    
    
    
    func selectLibros (parameters: [String: Any] ){
        AF.request("http://api-moviles-2.herokuapp.com/libros",parameters: parameters).response{
            response in
            debugPrint(response)
            if let data = response.data {
                
                do{
                    let libroResponse = try JSONDecoder().decode([LibroModel].self, from: data)
                    self.arrLibros.append(contentsOf: libroResponse)
                    
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
        
        
    }
    
    
     func selectLibrosAll (){
         AF.request("http://api-moviles-2.herokuapp.com/libros_all").response{
             response in
             debugPrint(response)
             if let data = response.data {
                 
                 do{
                     let libroResponse = try JSONDecoder().decode([LibroModel].self, from: data)
                     self.arrLibros.append(contentsOf: libroResponse)
                     
                 }catch let err{
                     print(err.localizedDescription)
                 }
             }
         }
         
         
     }
    
    func selectNotaDeterminadas (parameters: [String: Any], context: ViewController_Mis_Notas){
        AF.request("http://api-moviles-2.herokuapp.com/notas",parameters: parameters).response{
            response in
            debugPrint(response)
            if let data = response.data {
                
                do{
                    let notaResponse = try JSONDecoder().decode([NotasModel].self, from: data)
                    self.arrNotas.append(contentsOf: notaResponse)
                    context.notasAPI = self.arrNotas
                    context.tableView.reloadData()
                    print("Estoy en la primera------------------")
                }catch let err{
                    print(err.localizedDescription)
                    print("Estoy en la primera Error------------------")
                }
            }
        }
        
    }
    
    func selectNotaDeterminadas (parameters: [String: Any]){
        AF.request("http://api-moviles-2.herokuapp.com/notas",parameters: parameters).response{
            response in
            debugPrint(response)
            if let data = response.data {
                
                do{
                    let notaResponse = try JSONDecoder().decode([NotasModel].self, from: data)
                    self.arrNotas.append(contentsOf: notaResponse)
                }catch let err{
                    print(err.localizedDescription)
                }
            }
        }
        
    }
    
    func selectNotaEspecificas (parameters: [String: Any], context: ViewController_Todas_Notas){
           
           AF.request("http://api-moviles-2.herokuapp.com/notas_libro",parameters: parameters).response{
               response in
               debugPrint(response)
               if let data = response.data {
                   
                   do{
                       let notaResponse = try JSONDecoder().decode([NotasModel].self, from: data)
                       self.arrNotas.append(contentsOf: notaResponse)
                        context.notasAPI = self.arrNotas
                        context.table_view.reloadData()
                   }catch let err{
                       print(err.localizedDescription)
                   }
               }
           }
    }

    
    func selectNotaEspecificas (parameters: [String: Any]){
              
              AF.request("http://api-moviles-2.herokuapp.com/notas_libro",parameters: parameters).response{
                  response in
                  debugPrint(response)
                  if let data = response.data {
                      
                      do{
                          let notaResponse = try JSONDecoder().decode([NotasModel].self, from: data)
                          self.arrNotas.append(contentsOf: notaResponse)
                      }catch let err{
                          print(err.localizedDescription)
                      }
                  }
              }
       }
    func borrarNota (parameters: [String: Any]){
          
        AF.request("http://api-moviles-2.herokuapp.com/borrar_nota", method: .post, parameters: parameters).response{
            response in
            debugPrint(response)
        }
           
    }
    
    func borrarLibro (parameters: [String: Any], context: ViewController_Todas_Libretas){
           
        AF.request("http://api-moviles-2.herokuapp.com/borrar_libro", method: .post, parameters: parameters).response{
            response in
            debugPrint(response)
            context.collection_view.reloadData()
        }
           
    }
    
    
    func updateNotas (parameters: [String: Any]){
        AF.request("http://api-moviles-2.herokuapp.com/updateNota", method: .post, parameters: parameters).response{
                   response in
                   debugPrint(response)
               }
        
        
    }
    
    
}
